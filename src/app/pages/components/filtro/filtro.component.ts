import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-filtro',
  templateUrl: './filtro.component.html',
  styleUrls: ['./filtro.component.scss']
})
export class FiltroComponent implements OnInit {

  public formsFiltro = new FormGroup({});

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.criandoForms();
  }

  public criandoForms(): void {
    this.formsFiltro = this.fb.group({
      produto: [''],
      preco: [''],
      quantidade: ['']
    })
  }

}
